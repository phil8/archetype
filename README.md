# Archetype Capability

## Elevator Pitch
The archetype idea is to create project dependencies which bring standard tooling (npm scripts and dev dependencices) into a project.
It is then possible to update the tooling as standards move forward and it is easy to upgrade projects.

## History
There are some approaches to bootstrap new project from previous projects including
- using project templates
- using project generators 

## Archetype Capability
##### Problem
While project templates and generators are decent first steps there is a problem that the generator projects have no 'upgrade' path.  Once generated there is no way to update them.
This was something that was a concern of Walmart has they attempted to foster more reuse as described in this article [Achieving component reuse](https://medium.com/walmartlabs/how-to-achieve-reusability-with-react-components-81edeb7fb0e0)

##### A Solution
[This](http://www.electrode.io/docs/what_are_archetypes.html) describes that an archetype are packaged dependencies of a project to allow for project standardization during initial project creation AND supports change management/updating of this capability across a number of projects.  
When there will be a reasonable large number of projects we need this capability to allow components to be maintainable over time and not get old.

The Walmart archetype *Electron* might be a candidate framework to facilitate 'management' of a reasonble number of source projects.  However the functionality appears to be to restrictive for Quartile One needs.  However the Walmart archetype capability uses one capability in particular which can be used by Quartile One. Specifically it uses the [**builder**](https://www.npmjs.com/package/builder) task runner to facilitate centralized npm tastk definitions.

Builder effectively lets you define tasks and projects dependencies in a separate NPM project and this can then be installed as a dependency of the other project.  Effectively the new project can be reduced to a minimumum as most of the base functionality is pulled into it via the archetype dependency.

```Note that react does something similar and I am interested to find out if the approach is the same as builder as different.```

It is proposed builder is used to support development of QuartileOne centralized project archetypes.

Builder has been used to create a React UI Component archetype as demonstrated in these projects:
- [React Component Archetype Tasks](https://github.com/bayeslife/short-interval-control-archetype-react-component-) / [React Component Archetype Dependencies](https://github.com/bayeslife/short-interval-control-react-ui-component-dev)

and then consumed into these projects
- [React Assets UI Component](https://github.com/bayeslife/react-asset-component)
- [React Timeline UI Component](https://github.com/bayeslife/react-timeline-component)
- [React Gauge UI Component](https://github.com/bayeslife/react-gauge-component)

The effect of the archetype is to simplify the project SDLC structure as it is tasks and dependenencies are acquired from the archetype projects.

The archetype approach can be used for library projects , application projects, api projects and others.

```Question:  Do we make use archectype?  Do we builder as a way to make projects upgradeable?```

**Note** it is somewhat event more difficult to keep the archetype up to date as the change, testing cycle is even long.  Change archetype, publish, generate, install, run/test.

